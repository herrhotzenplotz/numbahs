AS=	nasm
AFLAGS=	-g -O0 -felf64 -D__`uname`__

SRCS=	main.S
PROG=	main

OBJS=	${SRCS:.S=.o}

.PHONY: all clean
all: ${PROG}

.SUFFIXES: .S .o
.S.o:
	${AS} ${AFLAGS} -o $@ $<

${PROG}: ${OBJS}
	${LD} -o ${PROG} ${OBJS}

clean:
	rm -f ${PROG} ${OBJS}
